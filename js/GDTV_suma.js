function getActualSchedule(maskId) {
    var info = EPG.getSearchResult(maskId);
    var scheduleEvents = [];
    for(var i=0;i<info.length;i++) {
        var p = info[i];
        var tempInfo = {
            "network_id" : p.NetworkID,
            "original_network_id" : p.NetworkID,
            "transport_stream_id" : -1,
            "service_id" : p.serviceID,
            "event_id" : p.eventID,
            "event_name" : p.name,
            "event_description" : p.description,
            "running_status" : p.status,
            "startDate" : p.date,
            "endDate" : p.date,
            "startTime" : p.startTime,
            "endTime" : p.endTime,
            "duration" : p.duration,
            "minAge" : p.minAge,
            "free_CA_mode" : p.isCA,
        };

        var temp = {};
        temp["info"] = p;
        temp["eventObj"] = {};
        temp["eventObj"] = tempInfo;
        scheduleEvents.push(temp);
    }
    return scheduleEvents;
};

function access(_class, _attr, _val) {
    if (_val == null) {
        var result = DataAccess.getInfo(_class, _attr);
        return result;
    } else {
        var result = DataAccess.setInfo(_class, _attr, _val);
        if (result) {
            return DataAccess.save(_class, _attr);
        } else {
            return 0;
        }
    }
};
suma_media = {
    media: null,
    mediaHandle: 0,
    playFlag:0,
    createMedia:function (fullScreen, left, top, width, height, source) {

        if(typeof  MediaPlayer == "undefined" ){
            box_log("123123");
            return;
        }
        var fullScreen = fullScreen;
        var left = left || 0;
        var top = top || 0;
        var width = width || 0;
        var height = height || 0;
        var paramsArr = [fullScreen, left, top, width, height];

        this.media = new MediaPlayer();
        this.mediaHandle = this.media.createPlayerInstance("Video", 2);

        this.media.position = paramsArr.join(",")//"0,50,144,620,321";
        this.media.source = source || "delivery://227000.6875.64QAM.101.800.600";//"delivery://227000.6875.64QAM.101.800.600";//;
        this.playFlag = 1;
        this.media.play();

        return this.media;
    },
    play: function (source) {
        this.media.source = source;

        if(this.playFlag){

           this.media.refresh();
           // this.stop();
           // this.media.play();
        }else{

            this.playFlag = 1;
            this.media.play();
        }
    },
    refresh: function(){
        this.media.refresh();
    },
    pause:function(){
        this.media.pause(1);
    },
    stop: function () {
        this.media.pause(0);
    },
    destory: function () {
        this.media.releasePlayerInstance(this.mediaHandle);
    }
}




