window.onunload = stopSmallAV;
var mediaPlayer;
var mediaID;
var createPlayerSuccess;
/*首页版块小视屏相关  start*/
function createMediaPlayer(left,top,width,height){//创建播放器对象
    mediaPlayer = new MediaPlayer();
    mediaID = mediaPlayer.getPlayerInstanceID();
    var flag = mediaPlayer.bindPlayerInstance(mediaID);
    createPlayerSuccess = flag == 0 ? true : false;
    if(createPlayerSuccess){
        var rect = new Rectangle(left,top,width,height);
        var flag1 = mediaPlayer.setVideoDisplayArea(rect);
        var flag2 = mediaPlayer.setVideoDisplayMode(0)
        var flag3 = mediaPlayer.refresh();
        GlobalVarManager.setItemStr("playType","DVB");
        GlobalVarManager.setItemStr("stopMode","0");
    }
}
function stopSmallAV(){//停止播放小视屏
    if(createPlayerSuccess){
        mediaPlayer.stop();
    }
}
function playSmallAV(){//播放小视屏，第一次进入则播放0频道，否则的话播放关机频道
    if(!createPlayerSuccess) return;
    var filterArray = [1000,1000];
    var valueArray = [1,2];
    var channelList = ChannelManager.filter(filterArray,valueArray);
    if(channelList.length == 0){
        //$("AVTips").innerText = "无节目，请重新搜索";
        //$("AVTips").style.visibility = "visible";
        return;
    }
    var currChannel = null;
    if(firstEnterIndex != "false"){
        var initPlayServiceId = GlobalVarManager.getItemStr("initPlayServiceId");
        var servcieId;
        for(var i = 0; i < channelList.length; i++){
            servcieId = channelList[i].getService().service_id;
            if(servcieId == initPlayServiceId){
                currChannel = channelList[i];
                break;
            }
        }
        if(currChannel){
            var location = currChannel.getService().getLocation();
            mediaPlayer.setMediaSource(location);//0频道不让编辑的，因此不用判断是否锁定
            return ;
        }
    }
    currChannel = ChannelManager.getShutDownChannel(128);
    if(!currChannel) currChannel = channelList[0];
    if(currChannel.type == 0x02 ){//广播频道
        //$("musicImg").src = "audio_bg.jpg";
        //$("musicBg").style.visibility = "visible";
    }
    else{
        //$("musicBg").style.visibility = "hidden";
    }
    var tableID = DataAccess.getUserPropertyTable("userInfo");
    var passwordEnable = DataAccess.getProperty(tableID, "passwordEnable");
    if(passwordEnable && currChannel.isLocked){//锁定的
        mediaPlayer.stop();
        //$("AVTips").style.visibility = "频道锁定";
        //$("AVTips").style.visibility = "visible";
    }
    else{
        var location = currChannel.getService().getLocation();
        mediaPlayer.setMediaSource(location);
    }
}