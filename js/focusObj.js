//公网版本
var contentParams = {
    "index": {
        "ttyw":708,//首页头条要闻
        "carousel":697//轮播图
    },
    "szyw":{
        "ypbd":707,//延平报道
        "ttyw":708,//头条要闻
        "msgz":709,//民生关注
        "bgt":718//曝光台
    },
    "rdzx":699,//热点资讯
    "jgdw":"COLUMN202006041132538239",//机关单位
    "xzfc":701,//乡镇风采
    "payp":{//平安延平
        "zxxx":714,//资讯信息
        "jdsk":715,//景点实况
    },
    "jyzc":703,//教育之窗
    "sxgl":704,//四贤故里
    "dyyj":{//党员远教
        "djdt":716,
        "dyjy":""//党员教育
    },
    "whys":706//文化艺术
};
//内网版本
var contentParams2 = {
    "index": {
        "ttyw":2096,//首页头条要闻
        "carousel":2104//轮播图
    },
    "szyw":{
        "ypbd":2095,//延平报道
        "ttyw":2096,//头条要闻
        "msgz":2097,//民生关注
        "bgt":2098//曝光台
    },
    "rdzx":2083,//热点资讯
    "jgdw":"COLUMN202006051105204465",//机关单位
    "xzfc":2090,//乡镇风采
    "payp":{//平安延平
        "zxxx":2105,//资讯信息
        "jdsk":715,//景点实况
    },
    "jyzc":2086,//教育之窗
    "sxgl":2087,//四贤故里
    "dyyj":{//党员远教
        "djdt":2083,
        "dyjy":2094//党员教育
    },
    "whys":2090//文化艺术
};

contentParams = typeof iPanel == "undefined" ?contentParams:contentParams2;


/*首页*/
//直播框
var videoObj = new FocusTables({
    left:247,
    top:0,
    width:490,
    height:276,
    name:"video",
    tpl:"bottomTpl",
    // background:"#F1F1F1",
    templateData:[
        {"title":"直播","left":0,"top":0,"width":490,"height":276},
    ],
    data:[{"title":"直播"}],
    create:function(){

    },
    updated:function(){


        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });


    },
    itemFocusOn:function (item) {
        this.listFocus.defaultFocusChange(item);
        this.listFocus.show();
    },
    itemFocusOut:function (item) {
        this.listFocus.hidden();
    },
    enter:function (item) {
        pageStorePush(window.location.href.split("?")[0],{pos:item.index,area:1,menu:menuObj.select_index});
        // alert(localStorage.getItem(requestConfig.ca_key))
        if(item.url){
            window.location.href = item.url + "?cid="+item.id+"&title="+item.name+"&ca="+getGlobalVar(requestConfig.ca_key);
        }
    }
});
//头条要闻
var ttywList=  new FocusTables({
    left:246,
    top:282,
    width:656,
    height:186,
    ttywPos:Q.getInt("ttywPos"),
    name:"index-ttyw",
    tpl:"listTpl",
    background:"images/index/ttyw/list-bg.png",
    listItem:{
        "row":4,
        "col":1,
        "width":596,
        "height":46,
        "space_left":0,
        "padding_left":50
    },
    data:[

    ],
    create:function(){
        var self = this;
        if(this.data.length!=0){
            self.setData(this.data);
            return;
        }



        request("content",{columnId:contentParams.index.ttyw,qry:2,"rows":8,"page":this.page+1},function (re) {

            // self.data = re["rows"];
            // console.log(re["rows"]);
            self.setData(re['rows']);

            if(self.ttywPos!=-1 && Q.getInt("ttxw")){
                self.itemGetFocus(Q.getInt("ttywPos"));
                self.ttywPos = -1;
            }
            // self.data = re["rows"];
        })
    },
    itemFocusOn:function (item) {


        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#ffffff";

    },
    itemFocusOut:function (item) {
        item.dom.style.background = "";
        item.dom.style.color = "#333333";

    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{"area":1,"ttxw":"1",menu:menuObj.index,indexPos:3,ttywPos:item.index});
    }
});
//轮播图{"title":"轮播图","left":742,"top":0,"width":398,"height":276},
var carouselObj = new FocusTables({
    left:742,
    top:0,
    width:398,
    height:276,
    name:"carousel",
    tpl:"carouselTpl",
    listItem:{
        width:12,
        height:12,
        row:1,
        col:10,
        // space_top:12,
        space_left:12,
        padding_top:17,
        padding_left:28
    },
    carouselPos:Q.getInt("carouselPos"),
    data:[
        {"name":"三明市生态新城系统解决方案","type":"news","cover":"images/index/carousel-0.png","titleimg":"images/index/carousel-0.png"},
        {"name":"园区动态","type":"news","cover":"images/xxfb/menu/1.png"},
        {"name":"园区风采","type":"teams","cover":"images/xxfb/menu/2.png"},
        {"name":"海报轮播图","type":"teams","cover":"images/xxfb/menu/3.png"},
        {"name":"海报","type":"news","cover":"images/xxfb/menu/4.png.png"}
    ],
    create:function(){
        var that = this;
        request("content",{qry:2,columnId:contentParams.index.carousel},function (re) {
            if(re["code"] == "000" && re["total"] > 0){

                that.setData(re["rows"]);
                var item  = that.data[0]
                $("carouseImg").src = item.titleimg||item.cover;
                if(that.carouselPos!=-1){

                    that.itemGetFocus(Q.getInt("carouselPos"));
                    that.carouselPos = -1;
                }
            }
        })
    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {

        item.dom.style.background = "#ffffff";
        $("carouseImg").src = item.titleimg||item.cover;

    },
    itemFocusOut:function (item) {
        item.dom.style.background = "#aaaaaa";
    },
    enter:function (item) {
        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{"area":1,"carousel":"1",menu:menuObj.index,indexPos:2,carouselPos:item.index});
    }
});
//首页*/
var indexObj = new FocusTables({
    left:0,
    top:8,
    width:1140,
    height:490,
    name:"index",
    tpl:"bottomTpl",
    indexPos:Q.getInt("indexPos"),
    // background:"#F1F1F1",
    templateData:[
        {"title":"海报1","left":0,"top":0,"width":240,"height":468},
        {"title":"直播", left:247,top:0, width:490, height:276,"cover":"images/tm.gif"},
        carouselObj,
        ttywList,
        {"title":"二维码","left":908,"top":282,"width":232,"height":186,"cover":""}
    ],
    data:[
        {"title":"海报1","cover":"images/index/poster1.png"},
        {"title":"直播","cover":"images/tm.gif","url":"ui://play.html"},
        {"title":"轮播图","cover":"images/tm.gif"},
        {"title":"头条要闻","cover":"images/tm.gif"},
        {"title":"二维码","cover":"images/index/code.png"}
        ],
    create:function(){


    },
    updated:function(){
        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
        createMediaPlayer(316,146,490,276);
        playSmallAV();
        if(this.indexPos){
            this.itemGetFocus(this.indexPos);
            this.indexPos = 0;

        }

    },
    itemFocusOn:function (item) {
        if(item.data==undefined){
            this.listFocus.defaultFocusChange(item)
        }
        this.listFocus.show();
    },
    itemFocusOut:function (item) {
        this.listFocus.hidden();
    },
    enter:function (item) {
        if(item.url){
            pageRedirectTo(item.url);
        }
        item.doSelect();
    }
});

//延平报道
var ypbdListObj = new FocusTables({
    left:230,
    top:1,
    width:910,
    height:490,
    name:"ypbdList",//图文
    tpl:"ypbdListTpl",
    "background":"#f1f1f1",
    listItem:{
        "row":2,
        "col":3,
        "width":272,
        "height":178,
        "space_top":53,
        "space_left":12,
        "padding_left":20,
        "padding_top":20
    },
    cid:contentParams.szyw.ypbd,
    ypbdPos:Q.getInt("ypbdPos"),
    data:[

    ],
    create:function(){
        if(this.ypbdPos!=-1 && Q.getDecoded("ypbdPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }
        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
    },
    updated:function(){

    },
    fetchListData:function(cid,getFocusFlag){

        var self = this;
        if(this.data.length>0&&this.page == 0){
            self.setData(this.data);
            return
        }

        request("content",{qry:2,columnId:this.cid,"rows":6,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/6);
                if(self.ypbdPos != -1 && Q.getDecoded("ypbdPos")!= "null"){

                    szywMenuObj.itemLoseFocus();
                    self.itemGetFocus(self.ypbdPos);
                    self.ypbdPos = -1;
                }
                if(getFocusFlag){

                    self.itemGetFocus();
                }
            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    itemFocusOn:function (item) {
        this.show();
        this.listFocus.defaultFocusChange(item);

        this.listFocus.show();
    },
    itemFocusOut:function (item) {

        this.listFocus.hidden();
    },
    enter:function (item) {
        if(item.type == 2){
            box_log("123");
            palyBackUrl = window.location.href.split("?")[0] +
                "?area="+1+"&area=2"+"&menu="+menuObj.index+"&szywMenu="+szywMenuObj.index+"&ypbdPos="+item.index+"&page="+this.page+"&bottom="+bottomObj.index;
            getDetailData(item.assetid);
        }else{
            pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,szywMenu:szywMenuObj.index,ypbdPos:item.index,page:this.page,bottom:1})
        }

    }
});
var msgzListObj = new FocusTables({
    left:230,
    top:1,
    width:910,
    height:490,
    name:"msgzList",//图文
    tpl:"listTpl",
    "background":"#f1f1f1",
    listItem:{
        "row":7,
        "col":1,
        "width":900,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    cid:contentParams.szyw.msgz,
    msgzPos:Q.getInt("msgzPos"),
    data:[

    ],
    create:function(){
        if(this.msgzPos!=-1 && Q.getDecoded("msgzPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }


    },
    updated:function(){


    },
    fetchListData:function(cid,getFocusFlag){
        var self = this;


        if(this.data.length>0&&this.page == 0){
            self.setData(this.data);
            return
        }

        request("content",{qry:2,columnId:this.cid,"rows":8,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/8);
                if(self.msgzPos != -1 && Q.getDecoded("msgzPos")!="null"){
                    szywMenuObj.itemLoseFocus();
                    self.itemGetFocus(self.msgzPos);
                    self.msgzPos = -1;
                }
                if(getFocusFlag){
                    self.itemGetFocus();

                }

            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    itemFocusOn:function (item) {
        this.show();
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#ffffff";

    },
    itemFocusOut:function (item) {
        item.dom.style.background = "";
        item.dom.style.color = "#333333";

    },
    enter:function (item) {
        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,szywMenu:szywMenuObj.index,msgzPos:item.index,page:this.page,bottom:2})
    }
});
var bgtListObj = new FocusTables({
    left:230,
    top:1,
    width:910,
    height:490,
    name:"bgtList",//图文
    tpl:"bgtListTpl",
    "background":"#f1f1f1",
    cid:contentParams.szyw.bgt,
    listItem:{
        "row":2,
        "col":3,
        "width":272,
        "height":178,
        "space_top":53,
        "space_left":12,
        "padding_left":20,
        "padding_top":20
    },
    data:[

    ],
    bgtPos:Q.getInt("bgtPos"),
    create:function(){
        if(this.bgtPos!=-1  && Q.getDecoded("bgtPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }
    },
    updated:function(){

        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
    },
    fetchListData:function(cid,getFocusFlag){
        var self = this;
        if(this.data.length>0&&this.page == 0){
            self.setData(this.data);
            return
        }
        request("content",{qry:2,columnId:this.cid,"rows":8,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/8);
                if(self.bgtPos != -1 && Q.getDecoded("bgtPos")!="null"){
                    szywMenuObj.itemLoseFocus();
                    self.itemGetFocus(self.bgtPos);
                    self.msgzPos = -1;
                }
                if(getFocusFlag){
                    self.itemGetFocus();
                }
            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    focusChange:function(item,last_item){
        this.listFocus.bigFocusChange(item,last_item);
        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        this.show();

        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),5);
        this.listFocus.show();
    },
    itemFocusOut:function (item) {
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),100);
        this.listFocus.hidden();
    },
    enter:function (item) {
        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,szywMenu:szywMenuObj.index,bgtPos:item.index,page:this.page,bottom:4})
    }
});
//今日头条
var jrttListObj = new FocusTables({
    left:240,
    top:1,
    width:910,
    height:490,
    name:"jrtt",
    tpl:"newsListTpl",//新闻列表
    cid:contentParams.szyw.ttyw,
    jrttPos:Q.getInt("jrttPos"),
    listItem:{
        "row":6,
        "col":1,
        "width":880,
        "height":48,
        "space_top":0,
        "space_left":0,
        "padding_left":10,
        "padding_top":140
    },
    data:[

    ],
    create:function(){
        if(this.jrttPos!=-1  && Q.getDecoded("jrttPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }
        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
        // this.fetchListData();

    },
    updated:function(){

    },
    fetchListData:function(cid,getFocusFlag){

        var self = this;
        if(this.data.length>0 &&this.page == 0){
            self.setData(this.data);
            return
        }

        request("content",{qry:2,columnId:this.cid,"rows":6,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/6);
                if(self.jrttPos != -1 && Q.getDecoded("jrttPos")!="null"){

                    szywMenuObj.itemLoseFocus();
                    self.itemGetFocus(self.jrttPos);
                    self.jrttPos = -1;
                }
            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    focusChange:function(item,last_item){
        if(item.index == 0){
            item.height = 182;
            item.top = 5;
        }
        this.listFocus.defaultFocusChange(item);
        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {

        this.show();
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),5);
        this.listFocus.show();
    },
    itemFocusOut:function (item) {
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),100);
        this.listFocus.hidden();
    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,szywMenu:szywMenuObj.index,jrttPos:item.index,page:this.page,bottom:bottomObj.index})

    }
});
//时政要闻
var szywMenuObj = new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"szywMenu",
    tpl:"szywMenuTpl",
    cover:"images/szyw/menu-bg.png",
    szywPos:Q.getInt("szywMenu"),

    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"title":"延平报道","listObj":ypbdListObj,"cid":contentParams.szyw.ypbd},
        {"title":"头条要闻","listObj":jrttListObj,"cid":contentParams.szyw.ttyw},
        {"title":"民生关注","listObj":msgzListObj,"cid":contentParams.szyw.msgz},
        {"title":"曝光台","listObj":bgtListObj,"cid":contentParams.szyw.bgt}
    ],

    create:function(){

    },
    updated:function(){

        setTimeout(function () {

            ypbdListObj.hidden();
            bgtListObj.hidden();
            msgzListObj.hidden();
            jrttListObj.hidden();
            if(szywMenuObj.szywPos!=-1){
                bottomObj.itemGetFocus(Q.getInt("bottom"));
                szywMenuObj.itemGetFocus(szywMenuObj.szywPos);
                szywMenuObj.szywPos = -1;
            }else{
                szywMenuObj.itemGetFocus(0);
            }
        },100)

    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        item.listObj.fetchListData();
        item.listObj.show();
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {
        item.listObj.hidden();
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item){
            bottomObj.templateData = item.template;
            bottomObj.fetchListData(item.id);
        }

    }
});
//热点资讯
var rdzxListObj = new FocusTables({
    left:0,
    top:0,
    width:1140,
    height:490,
    name:"rdzx",
    tpl:"listTpl",//新闻列表
    listItem:{
        "row":10,
        "col":1,
        "width":1140,
        "height":49,
        "space_top":0,
        "space_left":0,
        "padding_left":0,
        // "padding_top":248
    },
    background:"images/rdzx/list-bg.png",
    rdzxPos:Q.getInt("rdzxPos"),
    data:[
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},

    ],
    create:function(){
        if(this.rdzxPos!=-1  && Q.getDecoded("rdzxPos") != "null"){
            this.page = Q.getInt("rdzxPos");
        }else{
            this.page = 0;
        }
        this.fetchListData(0);
    },
    updated:function(){


    },
    fetchListData:function(getFocusFlag){
        var self = this;
        request("content",{qry:2,columnId:contentParams.rdzx,"rows":10,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/10);
                if(self.rdzxPos!=-1  && Q.getDecoded("rdzxPos") != "null"){
                    self.itemGetFocus(self.rdzxPos);
                    self.rdzxPos = -1;
                }
                if(getFocusFlag){
                    self.itemGetFocus();
                }
            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },

    itemFocusOn:function (item) {
        item.dom.style.background = "#6195F1";
        item.dom.style.color = "#ffffff";
        // $(item.dom.id+"-detail").style.display = "block";
        this.show();
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),5);

    },
    itemFocusOut:function (item) {
        item.dom.style.background = "";
        item.dom.style.color = "#333333";
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),100);

    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{pos:item.index,page:this.page})

        // window.location.href = "detail3.html?id="+item.id;
    }
});
/*机关单位*/
//一级菜单
var jgdwMenu =  new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"jgdwMenu",
    tpl:"szywMenuTpl",
    cover:"images/szyw/menu-bg.png",
    jgdwMenu:Q.getInt("jgdwMenu"),
    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    data:[

    ],
    create:function(){

        var self = this;
        request("column",{qry:2,columnCode:contentParams.jgdw,"rows":10,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/10);
            }
        })

    },
    updated:function(){
        var self = this;
        if(Q.getDecoded("jgdwMenu") != "null" && self.jgdwMenu!=-1){
            jgdwSubMenu.getData(this.data[self.jgdwMenu].columnCode)
            self.jgdwMenu = -1;
        }else{
            jgdwSubMenu.getData(this.data[0].columnCode)
        }

    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {

        jgdwSubMenu.getData(item.columnCode);

        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item){
            bottomObj.templateData = item.template;
            bottomObj.fetchListData(item.id);
        }

    }
});
//二级菜单
var jgdwSubMenu =  new FocusTables({
    left:231,
    top:0,
    width:145,
    height:416,
    name:"jgdwSubMenu",
    tpl:"szywMenuTpl",
    // cover:"images/szyw/menu-bg.png",
    jgdwSubMenu:Q.getDecoded("jgdwSubMenu"),
    listItem:{
        "row":7,
        "col":1,
        "width":145,
        "height":48,
        "space_top":20,
        "padding_top":10
    },
    data:[

    ],
    create:function(){



    },
    updated:function(){
        var self = this;
        if(Q.getDecoded("jgdwSubMenu") != "null" && self.jgdwSubMenu!=-1){
            jgdwList.fetchListData(this["data"][self.jgdwSubMenu].id)
            self.jgdwSubMenu = -1;
        }else{
            jgdwList.fetchListData(this["data"][0].id)
        }

    },
    getData:function(cid){
        var self = this;
        request("column",{qry:2,columnCode:cid,"rows":10,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/10);

            }
        })
    },

    itemFocusOn:function (item) {

        jgdwList.fetchListData(item.id)
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
        item.dom.style.borderRadius = "30px";
    },
    itemFocusOut:function (item) {


        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item){
            bottomObj.templateData = item.template;
            bottomObj.fetchListData(item.id);
        }

    }
});
//内容列表
var jgdwList=  new FocusTables({
    left:384,
    top:0,
    width:230,
    height:490,
    name:"jgdwList",
    tpl:"listTpl",
    jgdwListPos:Q.getInt("jgdwListPos"),
    listItem:{
        "row":7,
        "col":1,
        "width":756,
        "height":49,
        "space_left":0,
        "padding_left":0
    },
    data:[

      ],
    create:function(){
        if(this.jgdwListPos != -1 && Q.getDecoded("jgdwListPos")!="null"){
            this.page = this.jgdwListPos
        }else{
            this.page = 0;
        }
    },
    updated:function(){
        var self = this;

    },
    fetchListData:function(cid,getFocusFlag){

         var self = this;
         this.cid = cid;
         if(!this.cid){

             return;
         }
        request("content",{qry:2,columnId:this.cid,"rows":6,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/6);

                if(self.jgdwListPos != -1 && Q.getDecoded("jgdwListPos")!= "null"){
                    bottomObj.itemGetFocus(2);

                    self.itemGetFocus(self.jgdwListPos);
                    self.jgdwListPos = -1;
                }
                // if(getFocusFlag){
                //
                //     self.itemGetFocus();
                // }
            }
        })
    },
    itemFocusOn:function (item) {
        item.dom.style.background = "url(images/jgdw/list-bg.png)";
        // item.dom.style.color = "#1B638B";
    },
    itemFocusOut:function (item) {
        item.dom.style.background = "url(images/tm.gif)";
        // item.dom.style.color = "#ffffff";
    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,jgdwMenu:jgdwMenu.index,jgdwSubMenu:jgdwSubMenu.index,jgdwListPos:item.index,page:this.page,bottom:2})
    }
});
/*乡镇风采*/
//乡镇菜单
var xzfcMenu =  new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"xzfcMenu",
    tpl:"menuWithImgTpl",
    cover:"images/szyw/menu-bg.png",
    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":69.5,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"title":"赤门乡","cover":"images/xzfc/0.png"},
        {"title":"大横镇","cover":"images/xzfc/1.png"},
        {"title":"黄墩街道","cover":"images/xzfc/2.png"},
        {"title":"巨口乡","cover":"images/xzfc/3.png"},
        {"title":"来舟镇","cover":"images/xzfc/4.png"},
        {"title":"炉下镇","cover":"images/xzfc/5.png"},
        {"title":"茫荡山","cover":"images/xzfc/6.png"},
        {"title":"水乡太平","cover":"images/xzfc/7.png"},
        {"title":"水乡太平","cover":"images/xzfc/8.png"},
        {"title":"水乡太平","cover":"images/xzfc/9.png"},
        {"title":"水乡太平","cover":"images/xzfc/10.png"},
        {"title":"水乡太平","cover":"images/xzfc/11.png"},
        {"title":"水乡太平","cover":"images/xzfc/12.png"},
        {"title":"水乡太平","cover":"images/xzfc/13.png"},
        {"title":"水乡太平","cover":"images/xzfc/14.png"},
        {"title":"水乡太平","cover":"images/xzfc/15.png"},
        {"title":"水乡太平","cover":"images/xzfc/17.png"},
        {"title":"水乡太平","cover":"images/xzfc/18.png"},
        {"title":"水乡太平","cover":"images/xzfc/19.png"}
    ],
    create:function(){

        this.allData = [
                {"title":"赤门乡","cover":"images/xzfc/0.png"},
                {"title":"大横镇","cover":"images/xzfc/1.png"},
                {"title":"黄墩街道","cover":"images/xzfc/2.png"},
                {"title":"巨口乡","cover":"images/xzfc/3.png"},
                {"title":"来舟镇","cover":"images/xzfc/4.png","url":"http://10.199.255.247:8080/villageTemplet/tvPortal1.action?method=index&countryTypeId=262"},
                {"title":"炉下镇","cover":"images/xzfc/5.png","url":"http://10.199.255.251:8085/villageTemplet/village1157.html"},
                {"title":"茫荡山","cover":"images/xzfc/6.png"},
                {"title":"梅山街道","cover":"images/xzfc/7.png"},
                {"title":"南山镇","cover":"images/xzfc/8.png"},
                {"title":"水东街道","cover":"images/xzfc/9.png"},
                {"title":"水南街道","cover":"images/xzfc/10.png"},
                {"title":"四鹤街道","cover":"images/xzfc/11.png"},
                {"title":"塔前镇","cover":"images/xzfc/12.png"},
                {"title":"太平镇","cover":"images/xzfc/13.png"},
                {"title":"王台镇","cover":"images/xzfc/14.png"},
                {"title":"西芹镇","cover":"images/xzfc/15.png"},
                {"title":"陕阳镇","cover":"images/xzfc/16.png"},
                {"title":"洋后镇","cover":"images/xzfc/17.png"},
                {"title":"漳湖镇","cover":"images/xzfc/18.png","url":"http://10.199.255.247:8080/villageTemplet/tvPortal3.action?method=index&countryTypeId=304"},
                {"title":"紫云街道","cover":"images/xzfc/19.png"}
                ];
        this.total_page = Math.ceil(this.allData.length/7);
        this.page = 1;
    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    overBottom:function(){
        if(this.page<this.total_page){
            this.page++;
        }
        var newData = this.allData.slice((this.page-1)*7,this.page*7);
        this.setData(newData);
        this.itemGetFocus(0);
    },
    itemFocusOn:function (item) {
        console.log(item);
        $("xzfc-img").src = item.cover;
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item.url){
            pageRedirectTo(item.url);
        }
    }
});
/*平安延平*/
//内容列表 资讯信息
var paypList=  new FocusTables({
    left:230,
    top:0,
    width:900,
    height:490,
    name:"paypList",
    tpl:"listWithDateTpl",
    listItem:{
        "row":7,
        "col":1,
        "width":900,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    paypPos:Q.getInt("paypPos"),
    page_1_data:[],
    data:[],
    create:function(){
        if(self.paypPos!=-1  && Q.getDecoded("paypPos") != "null"){
            this.page = Q.getInt("page");
        }else{
            this.page = 0;
        }
        this.getData(contentParams.payp.rdzx);

    },
    getData:function(id){

        var self = this;
        if(this.page_1_data.length>0&&this.page==0){
            self.setData(self.page_1_data);
            return;
        }
        request("content",{qry:2,columnId:id,"rows":7,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                if(self.page == 0){
                    self.page_1_data = re["rows"];
                }

                if(self.paypPos!=-1  && Q.getDecoded("paypPos") != "null"){
                    self.itemGetFocus(self.paypPos);
                    self.paypPos = -1;
                }else{
                    if(this.page>0){
                        self.itemGetFocus();
                    }
                }

                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/7);


            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++;
            this.getData(contentParams.payp.zxxx)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.getData(contentParams.payp.zxxx)
        }
    },
    itemFocusOn:function (item) {
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#ffffff";
    },
    itemFocusOut:function (item) {
        item.dom.style.background = "";
        item.dom.style.color = "#333333";
    },
    enter:function (item) {
        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{pos:item.index,page:this.page})
    }
});

//一级菜单
var paypMenu =  new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"jgdwMenu",
    tpl:"szywMenuTpl",
    cover:"images/szyw/menu-bg.png",
    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"title":"资讯信息","id":contentParams.payp.zxxx},
        {"title":"景点实况","id":contentParams.payp.jdsk,"url":"jdsk.html"}
    ],
    create:function(){
        this.itemGetFocus(0);
    },
    itemFocusOn:function (item) {
        if(item.index == 0){
            paypList.getData(item.id);
        }else{

        }
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },

    enter:function (item) {
        if(item.url){
           pageRedirectTo(item.url,{"menu":menuObj.index});
        }

    }
});

/*四贤故里*/
var sxglList = new FocusTables({
    left:0,
    top:0,
    width:1140,
    height:490,
    name:"sxgl",//图文
    tpl:"imgListTpl",
    sxglPos:Q.getInt("sxglPos"),
    listItem:{
        "row":2,
        "col":4,
        "width":272,
        "height":178,
        "space_top":25,
        "space_left":12,
        "padding_left":20,
        "padding_top":20
    },
    data:[
        // {"title":"20181105"},
        // {"title":"20181105"},
        // {"title":"20181105"},
        // {"title":"20181105"},
        // {"title":"20181105"},
        // {"title":"20181105"},
        // {"title":"20181105"}
    ],
    create:function(){
        if(this.sxglPos!=-1 && Q.getDecoded("sxglPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }

        this.fetchListData();
    },
    updated:function(){
        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
    },
    fetchListData:function(getFocusFlag){

        var self = this;
        request("content",{qry:2,columnId:contentParams.sxgl,"rows":8,"page":this.page},function (re) {

            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/8);
                if(self.sxglPos!=-1&&Q.getDecoded("sxglPos")!="null"){
                    self.itemGetFocus(self.sxglPos);
                    self.sxglPos = -1;

                }

                if(getFocusFlag){
                    self.itemGetFocus();
                }


            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    focusChange:function(item,last_item){
        this.listFocus.bigFocusChange(item,last_item);
        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        this.show();

        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),5);
        this.listFocus.show();
    },
    itemFocusOut:function (item) {
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),100);
        this.listFocus.hidden();
    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,sxglPos:item.index,page:this.page})

    }
});

/*文化艺术*/
var whysList = new FocusTables({
    left:0,
    top:0,
    width:1140,
    height:490,
    name:"whys",//图文
    tpl:"listWithContentAndImgTpl",
    whysPos:Q.getInt("whysPos"),
    listItem:{
        "row":3,
        "col":1,
        "width":1140,
        "height":162,
        "space_top":0,
        "space_left":0,
        "padding_left":0,
        "padding_top":0
    },
    data:[
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"“副主任科员”再见！“提前退休”到来！公务员职务等迎大变革","content":"近日，《中华人民共和国公务员法（修订草案）》开始征求意见，征求意见的时间自2018年11月1日至2018年12月1日。"},

    ],
    create:function(){
        if(this.whysPos!=-1 && Q.getDecoded("whysPos") != "null"){
            this.page = Q.getInt('page');
        }else{
            this.page = 0;
        }
    },

    updated:function(){
        if(!this.listFocus){
            this.listFocus = new Focus({
                containner:this.containner,
                focusFolder:"images/focus20",
                td:16,
                padding_left:4.6,
                padding_top:4.6,
                spacing_left:4,
                spacing_top:4
            });
        }

    },

    fetchListData:function(getFocusFlag){
        var self = this;
        request("content",{qry:2,columnId:contentParams.whys,"rows":3,"page":this.page+1},function (re) {
            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/3);
                if(self.whysPos!=-1&&Q.getDecoded("whysPos")!="null"){
                    self.itemGetFocus(self.whysPos);
                    self.whysPos = -1;
                    self.page = 0;
                }
                if(getFocusFlag){
                    self.itemGetFocus();
                }

            }
        })
    },
    overBottom:function(){
        if(this.page<this.totalPage){
            this.page++
            this.fetchListData(1)
        }

    },
    overTop:function(){
        if(this.page>0){
            this.page--;
            this.fetchListData(1)
        }
    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        this.show();
        item.dom.style.background="url(images/whys/list-bg.png)";
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),5);

    },
    itemFocusOut:function (item) {
        // Util.showTitleForMarquee(item.title,$("szywList-"+item.index),100);
        item.dom.style.background="";
    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{area:1,menu:menuObj.index,whysPos:item.index,page:this.page})

        // window.location.href = "detail3.html?id="+item.id;
    }
});

/*党员远教*/
var dyyjMenu =  new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"dyyjMenu",
    tpl:"szywMenuTpl",
    cover:"images/szyw/menu-bg.png",
    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"title":"党建动态"},
        {"title":"党员教育","url":"http://10.215.0.39/fujiandangjianNew/indexNew.html"}
    ],
    create:function(){

    },
    updated:function(){
        dyyjList.getData();
    },
    itemFocusOn:function (item) {

        if( item.index == 0){

            dyyjList.getData();
        }else if(item.index == 1){
             pageRedirectTo(item.url);
        }

        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item){
            bottomObj.templateData = item.template;
            bottomObj.fetchListData(item.id);
        }
    }
});
var dyyjList =new FocusTables({
    left:230,
    top:0,
    width:900,
    height:490,
    name:"paypList",
    tpl:"listWithDateTpl",
    background:"images/dyyj/list-bg.png",
    listItem:{
        "row":7,
        "col":1,
        "width":900,
        "height":49,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"莆田市秀屿区劳动局","content":""},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"莆田市城建监察支队秀屿大队","content":""},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"湄洲妈祖祖庙消防队","content":""},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"莆田市秀屿区审计局","content":""},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"仙游县行政服务中心","content":""},
        {"titleimg":"images/demo/3.png","publishdate":"2020-06-01","title":"医疗保险管理中心","content":""},
    ],
    create:function(){


    },
    getData:function(getFocusFlag){
        var self = this;
        request("content",{qry:2,columnId:contentParams.dyyj.djdt,"rows":8,"page":this.page},function (re) {

            if(re["code"] == '000'){
                self.setData(re["rows"]);
                self.totalPage = parseInt(re["total"]/8);
                if(getFocusFlag){
                    self.itemGetFocus();
                }

            }
        })
    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#ffffff";
    },
    itemFocusOut:function (item) {
        item.dom.style.background = "";
        item.dom.style.color = "#333333";
    },
    enter:function (item) {

    }
});
/*投票*/
var tpList=  new FocusTables({
    left:0,
    top:0,
    width:230,
    height:400,
    name:"tpList",
    tpl:"tpListTpl",
    listItem:{
        "row":4,
        "col":1,
        "width":230,
        "height":52,
        "space_left":0,
        "space_top":15,
        "padding_left":873,
        "padding_top":180
    },
    data:[
        {"title":"候选人一"},
        {"title":"候选人二"},
        {"title":"候选人三"},
        {"title":"候选人四"},
    ],
    create:function(){

    },
    itemFocusOn:function (item) {
        item.dom.style.background = "#FFFFFF";
        item.dom.style.color = "#333F4B";
    },
    itemFocusOut:function (item) {
        item.dom.style.background = "#5E7E9E";
        item.dom.style.color = "#ffffff";
    },
    enter:function (item) {

    }
});
/*便民服务*/
var bmfwMenuObj = new FocusTables({
    left:0,
    top:0,
    width:230,
    height:416,
    name:"szywMenu",
    tpl:"szywMenuTpl",
    cover:"images/szyw/menu-bg.png",
    listItem:{
        "row":7,
        "col":1,
        "width":230,
        "height":68,
        "space_left":0,
        "padding_left":0
    },
    data:[
        {"title":"健康服务","listObj":jkfwList,"cid":contentParams.szyw.ypbd},
        {"title":"家政服务","listObj":jkfwList,"cid":contentParams.szyw.ttyw},
        {"title":"科技兴农","listObj":ypbdListObj,"cid":contentParams.szyw.msgz},
        {"title":"求职信息","listObj":bgtListObj,"cid":contentParams.szyw.bgt},
        {"title":"房产信息","listObj":bgtListObj,"cid":contentParams.szyw.bgt},
        {"title":"便民缴费","listObj":bgtListObj,"cid":contentParams.szyw.bgt,"url":"http://ixxgc3.xmiptv.net:8081/xmyygh/ticket/yygh-index.action"}

    ],

    create:function(){
        // ypbdListObj.fetchListData(contentParams.szyw.ypbd);
        // jrttListObj.fetchListData(contentParams.szyw.ttyw)


    },
    updated:function(){
        setTimeout(function () {
            // jkfwList.hidden();
            // jkfwList.hidden();
            // szywMenuObj.itemGetFocus(0)
        },100)

    },
    focusChange:function(item,last_item){

        if(last_item){
            this.itemFocusOut(last_item);
        }
        this.itemFocusOn(item);
    },
    itemFocusOn:function (item) {
        // item.listObj.fetchListData(item.cid);
        // item.listObj.show();
        item.dom.style.background = "#66AEEB";
        item.dom.style.color = "#FFFFFF";
    },
    itemFocusOut:function (item) {

        // item.listObj.hidden();
        item.dom.style.color = "#1B638B";
        item.dom.style.background = "";
    },
    enter:function (item) {
        if(item){
            bottomObj.templateData = item.template;
            bottomObj.fetchListData(item.id);
        }

    }
});

var jkfwList = new FocusTables({
    left:230,
    top:0,
    width:927,
    height:476,
    name:"rdzx",
    tpl:"jkfwTpl",//新闻列表
    templateData:[
        {"title":"健康咨询","left":"0","top":0,"width":280,"height":168},
        {"is_news":1,"left":300,"top":0,"width":600,"height":168},
        {"title":"广场舞","left":0,"top":192,"width":282,"height":276},
        {"title":"饮食","left":284,"top":192,"width":282,"height":276},
        {"title":"保健","left":568,"top":192,"width":282,"height":276}
        ],
    data:[
        {"cover":"images/bmfw/1.png","publishdate":"2020-06-01"},
        {"cover":"images/bmfw/gcw.png","title":"日常生活中糖尿病人的饮食注意事项","content":"1、主食一般以米、面为主，但是，我们比较喜欢粗杂粮，如燕麦、麦片、玉米面等，因为这些食物中较多的无机盐、维生素，又富含膳食纤维"},
        {"cover":"images/bmfw/gcw.png"},
        {"cover":"images/bmfw/ys.png"},
        {"cover":"images/bmfw/bj.png"}
    ],
    create:function(){
        this.fetchListData(0);
        this.page = Q.getInt('page');

    },
    updated:function(){
        // if(this.data[0].dom){
        //     $(this.data[0].dom.id+"-detail").style.display = "block";
        // }
        this.listFocus = new Focus({
            containner:this.containner,
            focusFolder:"images/focus20",
            td:16,
            padding_left:4.6,
            padding_top:4.6,
            spacing_left:4,
            spacing_top:4
        });
    },
    fetchListData:function(getFocusFlag){

    },


    itemFocusOn:function (item) {

        this.show();
        this.listFocus.show()
        this.listFocus.defaultFocusChange(item)
    },
    itemFocusOut:function (item) {
        this.listFocus.hidden()
    },
    enter:function (item) {

        pageRedirectTo("detail.html?id="+item.id+"&title=images/logo.png",{pos:item.index,page:this.page})

        // window.location.href = "detail3.html?id="+item.id;
    }
});